import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalHeader } from '../../class/APIHeader/globalHeader';
import { GlobalAPIEndpoint } from '../../class/APIURLdefine/globalURL';
import { HttpClient, HttpHeaders }    from '@angular/common/http';

@Injectable()
export class UsersService {

  constructor(
    private http: HttpClient,
    private getHeader: GlobalHeader,
    private getURL: GlobalAPIEndpoint
  ) { }

  private url = this.getURL.APIEndpoint()

  login(data): Observable<any>{
    const url = `${this.url}/users/login`;
    return this.http.post(url, data, { headers: this.getHeader.HTTPLoginHeader() })
  }

  registration(data): Observable<any>{
    const url = `${this.url}/users/registration`;
    return this.http.post(url, data, { headers: this.getHeader.HTTPLoginHeader() })
  }

  forgotPassword(data): Observable<any>{
    const url = `${this.url}/users/password/forgot`;
    return this.http.post(url, data, { headers: this.getHeader.HTTPLoginHeader() })
  }

  updatePassword(data): Observable<any>{
    const url = `${this.url}/users/password/update`;
    return this.http.put(url, data, { headers: this.getHeader.HTTPHeader() })
  }

  logoutPassword(): Observable<any>{
    var data = ""
    const url = `${this.url}/users/logout`;
    return this.http.post(url, data, { headers: this.getHeader.HTTPHeader() })
  }
  
  addMember(data): Observable<any>{
    const url = `${this.url}/users/members`;
    return this.http.post(url, data, { headers: this.getHeader.HTTPHeader() })
  }

  profile(): Observable<any>{
    const url = `${this.url}/users/profile`;
    return this.http.get(url, { headers: this.getHeader.HTTPHeader() })
  }

  dashboard(): Observable<any>{
    const url = `${this.url}/users/dashboard`;
    return this.http.get(url, { headers: this.getHeader.HTTPHeader() })
  }
}
