import { Injectable } from '@angular/core';
import { constant } from '../../../constants/constant';
import { UsersService } from '../../api/users/users.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class UsersModelService {

  userProfileInfo = new BehaviorSubject(null)
  userDashboardCounts = new BehaviorSubject(null)

  constructor(
    private usersService: UsersService,
    private router: Router,
  ) { }

  postLogin(formdata){
    var data = JSON.stringify(formdata)
    this.usersService.login(formdata)
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        localStorage.setItem(constant.TOKEN, response.data.token)
        this.router.navigate(['dashboard'])
      }
      else { }
    },
    error => { })
  }

  postRegistration(formdata){
    var data = JSON.stringify(formdata)
    this.usersService.registration(formdata)
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        
      }
      else { }
    },
    error => { })
  }

  postAddMember(formdata){
    var data = JSON.stringify(formdata)
    this.usersService.addMember(formdata)
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        
      }
      else { }
    },
    error => { })
  }

  getUserProfile(){
    this.usersService.profile()
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        this.userProfileInfo.next(response.data)
      }
      else { }
    },
    error => { })
  }

  getDashboardCounts(){
    this.usersService.dashboard()
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        this.userDashboardCounts.next(response.data)
      }
      else { }
    },
    error => { })
  }

  postUpdatePassword(formdata){
    var data = JSON.stringify(formdata)
    this.usersService.updatePassword(formdata)
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        
      }
      else { }
    },
    error => { })
  }

  postForgotPassword(formdata){
    var data = JSON.stringify(formdata)
    this.usersService.forgotPassword(formdata)
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){
        
      }
      else { }
    },
    error => { })
  }

  postLogoutUser(){
    this.usersService.logoutPassword()
    .subscribe(response => {
      if(response.status === constant.SUCCESSRESPONSE){        
        localStorage.removeItem(constant.TOKEN)
        localStorage.removeItem(constant.ROLECODE)
        
        this.router.navigate(['login'])
      }
      else { }
    },
    error => { })
  }

}
