import { TestBed, inject } from '@angular/core/testing';

import { UsersmodelService } from './usersmodel.service';

describe('UsersmodelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersmodelService]
    });
  });

  it('should be created', inject([UsersmodelService], (service: UsersmodelService) => {
    expect(service).toBeTruthy();
  }));
});
