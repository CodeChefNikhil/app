import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Injectable()
export class SnackbarService {

  constructor(
    public snackBar: MatSnackBar,
  ) { }

  successToaster(msg, action){
    this.snackBar.open(msg, action, {
      panelClass: ['custom-success-toaster'],
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  errorToaster(msg, action){
    this.snackBar.open(msg, action, {
      panelClass: ['custom-error-toaster'],
      duration: 2000,
      verticalPosition: 'top',
    });
  }

  warningToaster(msg, action){
    this.snackBar.open(msg, action, {
      panelClass: ['custom-warning-toaster'],
      duration: 2000,
      verticalPosition: 'top',
    });
  }
}