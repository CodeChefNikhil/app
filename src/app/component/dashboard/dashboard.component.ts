import { Component, OnInit } from '@angular/core';
import { TitleServiceClass } from '../../class/titleService/titleService';
import { constant } from '../../../constants/constant';
import { UsersModelService } from '../../model/users/usersmodel.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ UsersModelService ]
})
export class DashboardComponent implements OnInit {

  counts: any;
  
  constructor(
    private titleServiceClass: TitleServiceClass,
    private usersModelService: UsersModelService
  ) { }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_DASHBOARD)
  }

  dashboard(){
    this.usersModelService.getDashboardCounts()
    this.counts = this.usersModelService.userDashboardCounts
  }

}
