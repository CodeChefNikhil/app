import { Component, OnInit } from '@angular/core';
import { constant } from '../../../../constants/constant';
import { TitleServiceClass } from '../../../class/titleService/titleService';
import { UsersModelService } from '../../../model/users/usersmodel.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ UsersModelService ]
})
export class ProfileComponent implements OnInit {

  userInfo: any;

  constructor(
    private titleServiceClass: TitleServiceClass,
    private usersModelService: UsersModelService
  ) { }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_PROFILE)

    this.userProfile()
  }

  userProfile(){
    this.usersModelService.getUserProfile()
    this.userInfo = this.usersModelService.userProfileInfo
  }

}
