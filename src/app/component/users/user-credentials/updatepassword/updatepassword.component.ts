import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersModelService } from '../../../../model/users/usersmodel.service';
import { constant } from '../../../../../constants/constant';
import { TitleServiceClass } from '../../../../class/titleService/titleService';

@Component({
  selector: 'app-updatepassword',
  templateUrl: './updatepassword.component.html',
  styleUrls: ['./updatepassword.component.css'],
  providers: [ UsersModelService ]
})
export class UpdatepasswordComponent implements OnInit {

  validateForm: any;
  hidePass: boolean = true;
  hideConfPass: boolean = true;
  oldHidePass: boolean = true;


  constructor(
    private usersModelService: UsersModelService,
    private titleServiceClass: TitleServiceClass
  ) {
    this.validateFormOnInit()
  }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_UPDATEPASSWORD)
  }

  validateFormOnInit() {
    this.validateForm = new FormGroup({
      'newPassword': new FormControl('', [
        Validators.minLength(6)
      ]),
      'cPassword': new FormControl('', [
        Validators.minLength(6)
      ]),
      'oldPassword': new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
    }, this.passwordValidator);
  }

  passwordValidator(g: FormGroup) {
    return g.get('newPassword').value === g.get('cPassword').value
      ? null : { 'mismatch': true };
  }


  onSubmit() {
    var formdata = this.validateForm.value
    this.usersModelService.postUpdatePassword(formdata)
  }

}
