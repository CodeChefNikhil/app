import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersModelService } from '../../../../model/users/usersmodel.service';
import { Router } from '@angular/router';
import { TitleServiceClass } from '../../../../class/titleService/titleService';
import { constant } from '../../../../../constants/constant';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
  providers: [ UsersModelService ]
})
export class ForgotpasswordComponent implements OnInit {

  validateForm: any;

  constructor(
    private usersModelService: UsersModelService,
    private router: Router,
    private titleServiceClass: TitleServiceClass
  ) {
    this.validateFormOnInit()
   }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_FORGOTPASSWORD)
  }

  validateFormOnInit(){
    this.validateForm = new FormGroup({
      'email': new FormControl('', []),
    });
  }

  onSubmit() {
    var formdata = this.validateForm.value
    this.usersModelService.postForgotPassword(formdata)
  }

}
