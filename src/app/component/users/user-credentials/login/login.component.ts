import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersModelService } from '../../../../model/users/usersmodel.service';
import { Router } from '@angular/router';
import { TitleServiceClass } from '../../../../class/titleService/titleService';
import { constant } from '../../../../../constants/constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ UsersModelService ]
})
export class LoginComponent implements OnInit {

  validateForm: any;

  // password icon style
  hidePass: boolean = true;

  constructor(
    private usersModelService: UsersModelService,
    private router: Router,
    private titleServiceClass: TitleServiceClass
  ) {
    this.validateFormOnInit()
   }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_LOGIN)
  }

  validateFormOnInit(){
    this.validateForm = new FormGroup({
      'name': new FormControl('', []),
      'password': new FormControl('', [
        Validators.minLength(6)
      ]),
    });
  }

  onSubmit() {
    var formdata = this.validateForm.value
    // this.usersModelService.postLogin(formdata)
    this.router.navigate(['dashboard'])
  }

}
