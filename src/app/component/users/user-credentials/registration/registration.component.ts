import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersModelService } from '../../../../model/users/usersmodel.service';
import { constant } from '../../../../../constants/constant';
import { TitleServiceClass } from '../../../../class/titleService/titleService';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [ UsersModelService ]
})
export class RegistrationComponent implements OnInit {

  validateForm: any;
  hidePass: boolean = true;
  hideConfPass: boolean = true;


  constructor(
    private usersModelService: UsersModelService,
    private titleServiceClass: TitleServiceClass
  ) {
    this.validateFormOnInit()
  }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_REGISTRATION)
  }

  validateFormOnInit() {
    this.validateForm = new FormGroup({
      'firstName': new FormControl('', [
        Validators.pattern('[a-zA-Z]{1,}')
      ]),
      'middleName': new FormControl('', [
        Validators.pattern('[a-zA-Z]{1,}')
      ]),
      'lastName': new FormControl('', [
        Validators.pattern('[a-zA-Z]{1,}')
      ]),
      'email': new FormControl('', []),
      'mobile': new FormControl('', [
        Validators.pattern('[1-9][0-9]{9}')
      ]),
      'cPassword': new FormControl('', [
        Validators.minLength(6)
      ]),
      'password': new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
    }, this.passwordValidator);
  }

  passwordValidator(g: FormGroup) {
    return g.get('password').value === g.get('cPassword').value
      ? null : { 'mismatch': true };
  }


  onSubmit() {
    var formdata = this.validateForm.value
    this.usersModelService.postRegistration(formdata)
  }

}
