import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersModelService } from '../../../model/users/usersmodel.service';
import { constant } from '../../../../constants/constant';
import { TitleServiceClass } from '../../../class/titleService/titleService';

@Component({
  selector: 'app-addmember',
  templateUrl: './addmember.component.html',
  styleUrls: ['./addmember.component.css'],
  providers: [ UsersModelService ]
})
export class AddmemberComponent implements OnInit {

  validateForm: any;

  constructor(
    private usersModelService: UsersModelService,
    private titleServiceClass: TitleServiceClass
  ) {
    this.validateFormOnInit()
  }

  ngOnInit() {
    this.titleServiceClass.setTitleMethod(constant.TITLE_ADDMEMBERS)
  }

  validateFormOnInit() {
    this.validateForm = new FormGroup({
      'firstName': new FormControl('', [
        Validators.pattern('[a-zA-Z]{1,}')
      ]),
      'middleName': new FormControl('', [
        Validators.pattern('[a-zA-Z]{1,}')
      ]),
      'lastName': new FormControl('', [
        Validators.pattern('[a-zA-Z]{1,}')
      ]),
      'email': new FormControl('', []),
      'mobile': new FormControl('', [
        Validators.pattern('[1-9][0-9]{9}')
      ]),
      'department': new FormControl('', []),
      'designation': new FormControl('', []),
    });
  }

  onSubmit() {
    var formdata = this.validateForm.value
    this.usersModelService.postAddMember(formdata)
  }

}
