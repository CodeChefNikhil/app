import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { constant } from '../../../../constants/constant';
import { UsersModelService } from '../../../model/users/usersmodel.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [ UsersModelService ]
})
export class HeaderComponent implements OnInit {

  menuText: string;
  
  // menubar configuration provide by angular material
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
    private usersModelService: UsersModelService,

    // menubar configuration provide by angular material
    // do not modify content
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.menuText = constant.MENU_BAR
  }


  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logoutUser(){
    this.usersModelService.postLogoutUser()
  }
}