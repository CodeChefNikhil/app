import { Component, OnInit } from '@angular/core';
import { constant } from '../../../../constants/constant';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  companyName: string;
  copyrightYear: string;
  verion: string;

  constructor() { }

  ngOnInit() {
    this.companyName = constant.FOOTER_COMPANYNAME;
    this.copyrightYear = constant.FOOTER_CPYEAR;
    this.verion = constant.FOOTER_VERSION;
  }

}
