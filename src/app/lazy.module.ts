import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule, NgModel, NgForm, FormGroup } from '@angular/forms';


// auth-guards module
import { UserLoggedInAuthGuardService } from './class/authguard/user-logged-in/user-logged-in.service';
import { UserNotLoginAuthGuardService } from './class/authguard/user-not-login/user-not-login.service';

// angular material 
import { MatCardModule, MatButtonModule, MatSidenavModule, MatToolbarModule, 
  MatMenuModule, MatListModule, MatIconModule, MatInputModule, MatSelectModule, 
  MatSnackBarModule } from '@angular/material'
import { MediaMatcher } from '@angular/cdk/layout';

// component modules
import { LandingPageComponent } from './component/navigation/landingpage/landingpage.component';
import { LoginComponent } from './component/users/user-credentials/login/login.component';
import { RegistrationComponent } from './component/users/user-credentials/registration/registration.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { FooterComponent } from './component/navigation/footer/footer.component';
import { HeaderComponent } from './component/navigation/header/header.component';
import { ProfileComponent } from './component/users/profile/profile.component';
import { AddmemberComponent } from './component/users/addmember/addmember.component';
import { ForgotpasswordComponent } from './component/users/user-credentials/forgotpassword/forgotpassword.component';
import { UpdatepasswordComponent } from './component/users/user-credentials/updatepassword/updatepassword.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent, 
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'user/profile', component: ProfileComponent },
      { path: 'add/member', component: AddmemberComponent },
      { path: 'password/update', component: UpdatepasswordComponent }
    ]
  },

  { path: 'login', component: LoginComponent },
  { path: 'login/session/expire', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'password/forgot', component: ForgotpasswordComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    MatCardModule, MatButtonModule, MatSidenavModule, MatButtonModule, 
    MatToolbarModule, MatMenuModule, MatListModule, MatIconModule, MatInputModule,
    MatSelectModule, MatSnackBarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LandingPageComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegistrationComponent,
    DashboardComponent,
    ProfileComponent,
    AddmemberComponent,
    ForgotpasswordComponent,
    UpdatepasswordComponent,
  ],
  providers: [
    MediaMatcher
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LazyModule { }
