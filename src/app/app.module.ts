import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule, NgModel, NgForm, FormGroup } from '@angular/forms';

// class
import { HttpInterceptorService } from './class/interceptor/httpinterceptor.service';
import { GlobalAPIEndpoint } from './class/APIURLdefine/globalURL';
import { GlobalHeader } from './class/APIHeader/globalHeader';
import { TitleServiceClass } from './class/titleService/titleService';

// angular material 

import { MatSnackBarModule } from '@angular/material'

// auth-guards module
import { UserLoggedInAuthGuardService } from './class/authguard/user-logged-in/user-logged-in.service';
import { UserNotLoginAuthGuardService } from './class/authguard/user-not-login/user-not-login.service';
import { ValidateUserService } from './class/authguard/validateuser/validateuser.service';

// service modules
import { UsersService } from './api/users/users.service';

// component modules
import { AppComponent } from './app.component';
import { PagenotfoundComponent } from './component/false-route/pagenotfound/pagenotfound.component';
import { SnackbarService } from './material/snackbar/snackbar.service';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '', loadChildren: './lazy.module#LazyModule'},

  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    FormsModule, ReactiveFormsModule, MatSnackBarModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ 
    // classes
    GlobalAPIEndpoint, GlobalHeader, TitleServiceClass,

    // to show alert message
    SnackbarService,
    
    // HTTP interceptor
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }, 

    // service
    UsersService, UserLoggedInAuthGuardService, UserNotLoginAuthGuardService, 
    ValidateUserService,    
   ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
