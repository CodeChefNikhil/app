import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, 
  RouterStateSnapshot, CanActivateChild, Router } from '@angular/router';
import { ValidateUserService } from '../validateuser/validateuser.service';

@Injectable()
export class UserLoggedInAuthGuardService {

  constructor(
    private validateUserService: ValidateUserService,
    private router: Router
  ){}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if(!this.validateUserService.UserLoggedIn()){
      this.router.navigate(['/login/session/expire']);
      return false;  
    }
    
    return true
  }

}
