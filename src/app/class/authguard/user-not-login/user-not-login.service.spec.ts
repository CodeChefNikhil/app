import { TestBed, inject } from '@angular/core/testing';

import { UserNotLoginService } from './user-not-login.service';

describe('UserNotLoginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserNotLoginService]
    });
  });

  it('should be created', inject([UserNotLoginService], (service: UserNotLoginService) => {
    expect(service).toBeTruthy();
  }));
});
