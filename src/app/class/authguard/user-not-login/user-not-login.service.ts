import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, 
  RouterStateSnapshot, CanActivateChild, Router } from '@angular/router';
import { ValidateUserService } from '../validateuser/validateuser.service';

@Injectable()
export class UserNotLoginAuthGuardService implements CanActivate {

  constructor(
    private validateUserService: ValidateUserService,
    private router: Router
  ){}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if(!this.validateUserService.UserNotLogin()){
      this.router.navigate(['/dashboard']);
      return false;
    }

    return true;
  }

}
