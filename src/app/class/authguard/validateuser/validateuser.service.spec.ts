import { TestBed, inject } from '@angular/core/testing';

import { ValidateuserService } from './validateuser.service';

describe('ValidateuserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidateuserService]
    });
  });

  it('should be created', inject([ValidateuserService], (service: ValidateuserService) => {
    expect(service).toBeTruthy();
  }));
});
