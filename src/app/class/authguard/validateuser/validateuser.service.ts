import { Injectable } from '@angular/core';
import { constant } from '../../../../constants/constant';

@Injectable()
export class ValidateUserService {

  verifyToken: any;
  verifyAppCode: any;
  isUserLoggedInStatus: any;

  constructor() { }

  UserNotLogin(){
    this.verifyToken = localStorage.getItem(constant.TOKEN);
    this.verifyAppCode = localStorage.getItem(constant.APPCODE);

    if (this.verifyToken && this.verifyAppCode) {
      return this.isUserLoggedInStatus = false;
    }
    else if (this.verifyToken && !this.verifyAppCode) {
      return this.isUserLoggedInStatus = true;
    }
    return this.isUserLoggedInStatus = true;
  }

  UserLoggedIn(){
    this.verifyToken = localStorage.getItem(constant.TOKEN);

    if (this.verifyToken) {
      return this.isUserLoggedInStatus = true;
    }
    return this.isUserLoggedInStatus = false;
  }

}
