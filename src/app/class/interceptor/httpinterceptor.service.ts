import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, 
  HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { SnackbarService } from '../../material/snackbar/snackbar.service'
import { constant } from '../../../constants/constant';
import 'rxjs/add/operator/do';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor{

  constructor(
    private router: Router,
    private snackBarService: SnackbarService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          
          // do stuff with response if you want
          if(event.body.statusCode == '401'){
            this.router.navigate(['login/session/expire']);
          }

          if(event.body.status == 'success'){
            this.snackBarService.warningToaster(event.body.message, '')
          }

        }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // do something if HTTP status change or unauthorize
        }

        this.snackBarService.warningToaster(constant.SOMETHINGWENTWRONG, '')
      }
    });
  }

}
