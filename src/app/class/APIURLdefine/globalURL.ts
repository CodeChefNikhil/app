import { environment } from '../../../environments/environment';

export class GlobalAPIEndpoint {

    url: string;

    APIEndpoint(){
        this.url = environment.ServerBaseUrl+environment.Api_V1_BasePath;

        return this.url;
    }

}