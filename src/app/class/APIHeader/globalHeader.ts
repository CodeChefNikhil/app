import { HttpClient, HttpHeaders } from '@angular/common/http';
import { constant } from '../../../constants/constant';
import { Headers, Http, RequestOptions } from '@angular/http';

export class GlobalHeader {

    appCode: any;
    token: any;
    headers: any;

    constructor() { }

    HTTPHeader() {
        this.appCode = localStorage.getItem(constant.APPCODE);
        this.token = localStorage.getItem(constant.TOKEN);

        this.headers = new HttpHeaders({ 'Content-Type': 'application/json', 'appInstanceCode': '' + this.appCode, 'token': '' + this.token });
        return this.headers;
    }

    HTTPLoginHeader() {
        this.appCode = localStorage.getItem(constant.APPCODE);

        if (this.appCode != null) { }
        else {
            this.appCode = this.generateGUID()
            localStorage.setItem(constant.APPCODE, this.appCode)
        }

        this.headers = new HttpHeaders({ 'Content-Type': 'application/json', 'appInstanceCode': '' + this.appCode });
        return this.headers;
    }

    HTTPFileUploadHeader() {
        this.appCode = localStorage.getItem(constant.APPCODE);
        this.token = localStorage.getItem(constant.TOKEN);

        this.headers = new HttpHeaders({ 'appInstanceCode': '' + this.appCode, 'token': '' + this.token });
        return this.headers;
    }

    generateGUID = function (): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}