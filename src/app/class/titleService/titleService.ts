import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { constant } from "../../../constants/constant";

@Injectable()
export class TitleServiceClass {

    constructor(
        private title: Title
    ) { }

    setTitleMethod(titleName){
        this.title.setTitle(constant.TITLE + titleName);
    }
}