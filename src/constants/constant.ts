export const constant = {
  
  TOKEN : 'token',
  APPCODE : 'appCode',
  ROLECODE : 'role',
  
  // global constant
  SOMETHINGWENTWRONG : "Something went wrong, please try again",

  // footer part constant
  FOOTER_COMPANYNAME: "Project",
  FOOTER_VERSION: "v.0.0.0",
  FOOTER_CPYEAR: "2017",

  // numeric check define code at DataBase
  MALECODE : '111',
  ADMINROLECODE : '112',

  // check response status
  SUCCESSRESPONSE : 'success',
  SUCCESSCODE200 : '200',

  // menu bar title
  MENU_BAR: "Project",
  
  // title pages
  TITLE: 'CROSS - ',
  TITLE_LOGIN: 'Login',
  TITLE_REGISTRATION: 'Registration',
  TITLE_UPDATEPASSWORD: 'Update Password',
  TITLE_FORGOTPASSWORD: 'Forgot Password',
  TITLE_PROFILE: "Profile",
  TITLE_ADDMEMBERS: 'Add Member',
  TITLE_DASHBOARD: "Dashboard",

};